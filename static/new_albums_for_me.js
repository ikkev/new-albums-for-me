$(document).ready(function() {
    function get_albums() {
        $.ajax({
            method: "POST",
            url: "/get_albums",
            data: { lastfm_user: $("#input_lastfm_user").val() },
            beforeSend: function() {
                $("#albums").html('');
                $('#input_lastfm_user').prop('disabled', true);
                $('#search_button').prop('disabled', true);
                $('#loader').css('display', 'block')
            },
            complete: function(){
                $('#loader').css('display', 'none')
                $('#search_button').prop('disabled', false);
                $('#input_lastfm_user').prop('disabled', false);
            }
        })
        .done(function(msg) {
            $("#albums").html(msg);
        });
    }

    $("#search_button").click(function(){
        get_albums();
    });

    $('#input_lastfm_user').keypress(function (e) {
        if (e.which == 13) {
            get_albums();
            return false;
        }
    });
});