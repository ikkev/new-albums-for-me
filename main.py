import config
import difflib
import json
import requests
from datetime import datetime
from flask import Flask, render_template, request
from flask_cache import Cache

app = Flask(__name__)
cache = Cache(app, config={'CACHE_TYPE': 'simple'})

LAST_FM_API_BASE_URL = 'http://ws.audioscrobbler.com/2.0/'
SPOTIFY_API_BASE_URL = 'https://api.spotify.com/v1/'
NUM_TOP_ARTISTS = 50
DEFAULT_COUNTRY = 'NL'


@app.route("/")
def main():
    return render_template(
        'homepage.html'
    )


@app.route("/get_albums", methods=['POST'])
def get_albums():
    top_artists = get_top_artists(request.form['lastfm_user'])

    top_artist_albums = []

    for top_artist in top_artists:
        artist_spotify_id = get_artist_spotify_id(top_artist['name'])
        if not artist_spotify_id:
            continue
        artist_albums = get_albums_of_artists(artist_spotify_id, top_artist['name'])
        top_artist_albums = top_artist_albums + artist_albums

    # Sort the list of albums of the top artists
    new_albums = sorted(top_artist_albums, key=lambda k: k['release_date'], reverse=True)
    return render_template(
        'new_albums.html',
        new_albums=new_albums
    )


def get_country_of_visitor():
    ip = request.remote_addr
    country = requests.get('http://ipinfo.io/' + ip + '/country').content

    if not len(country) == 2:
        return DEFAULT_COUNTRY
    return country


def get_top_artists(last_fm_user):
    response_json = requests.get(
        LAST_FM_API_BASE_URL +
        '?method=user.gettopartists'
        '&user=' + last_fm_user +
        '&limit=' + str(NUM_TOP_ARTISTS) +
        '&api_key=' + config.LAST_FM_API_KEY +
        '&format=json'
    )
    response_dict = json.loads(response_json.content.decode('utf-8'))

    return response_dict['topartists']['artist']


@cache.memoize(timeout=86400)
def get_artist_spotify_id(artist_name):
    # Search on Spotify based on artist name
    response_json = requests.get(
        SPOTIFY_API_BASE_URL +
        'search?q=' + artist_name +
        '&type=artist',
        timeout=5
    )
    response_dict = json.loads(response_json.content.decode('utf-8'))

    found_spotify_artists = {}
    for item in response_dict['artists']['items']:
        if item['name'].lower() == artist_name.lower():
            # Found an artist on spotify with the exact same name, return the id
            return item['id']

        found_spotify_artists[item['name'].lower()] = item['id']

    # Find the most similar artist
    similar_artist_names = difflib.get_close_matches(artist_name, found_spotify_artists.keys())
    if len(similar_artist_names) > 0:
        return found_spotify_artists[similar_artist_names[0]]

    # No similar artists at all found
    return None


@cache.memoize(timeout=86400)
def get_albums_of_artists(artist_spotify_id, last_fm_artist_name):
    country = get_country_of_visitor()

    # Get spotify albums of artist
    albums_response_json = requests.get(
        SPOTIFY_API_BASE_URL + 'artists/' + artist_spotify_id + '/albums'
        '?album_type=album'
        '&market=' + country,
        timeout=2
    )
    albums_response_dict = json.loads(albums_response_json.content.decode('utf-8'))

    album_spotify_ids = [x['id'] for x in albums_response_dict['items']]
    album_ids_string = ','.join(album_spotify_ids)
    albums_info_response_json = requests.get(
        SPOTIFY_API_BASE_URL + 'albums'
        '?ids=' + album_ids_string +
        '&market=' + country,
        timeout=2
    )
    albums_info_response_dict = json.loads(albums_info_response_json.content.decode('utf-8'))

    spotify_albums = []
    for album in albums_response_dict['items']:
        album_extra_info = next((item for item in albums_info_response_dict['albums'] if item['id'] == album['id']), None)

        # convert all release dates to same format
        if album_extra_info['release_date_precision'] == 'day':
            album_extra_info['release_date'] = album_extra_info['release_date']
        elif album_extra_info['release_date_precision'] == 'month':
            album_extra_info['release_date'] += '-01'
        elif album_extra_info['release_date_precision'] == 'year':
            album_extra_info['release_date'] += '-01-01'
        else:
            raise Exception('Unknown release_date_precision')

        spotify_album = {
            'spotify_id': album['id'],
            'name': album['name'],
            'artist': last_fm_artist_name,
            'url': album['external_urls']['spotify'],
            'image_url': album['images'][1]['url'],  # FIXME using index 1 is scary
            'release_date': datetime.strptime(album_extra_info['release_date'], '%Y-%m-%d'),
            'release_date_precision': album_extra_info['release_date_precision']
        }
        spotify_albums.append(spotify_album)

    return spotify_albums


if __name__ == "__main__":
    app.run(host='0.0.0.0')
