# New Albums For Me #

Website to see an overview of the newest albums of your top artists on Last FM

![rsz_2016-08-14_18_27_53-localhost_5000.jpg](https://bitbucket.org/repo/9Bq8Ko/images/759214029-rsz_2016-08-14_18_27_53-localhost_5000.jpg)

### Installation ###

* Install the python packages in requirements.txt
* * e.g. 'pip install requirements.txt'
* Add your Last FM API key to config.py (see http://www.last.fm/api/authentication)
* Run main.py
* * e.g. 'python main.py'
* Go to localhost:5000

### Todo ###

* Show nice error when API key is wrong
# Progress bar (top artists x/50)
* Make sure all album cover images are the same size
* Add unit tests
* Make it faster!
* Optimize the styling for mobile
